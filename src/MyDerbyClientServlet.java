import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet("/MyDerbyClientServlet")
public class MyDerbyClientServlet extends HttpServlet {

    //There is a connection pool in glassfish server named DerbyPool
    @Resource(name="java:global/DerbyPool")
    DataSource ds;
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Connection myPooledConnection = ds.getConnection();
            Statement statement = myPooledConnection.createStatement();
            
            //There is a table in database named Products
            //Idea knows that this table exists
            //This line breaks HTML-page response
            ResultSet set = statement.executeQuery("select * from PRODUCTS");

            PrintWriter writer = response.getWriter();
            writer.println("<html><body><h1>Time to buy it!</h1>");
            while (set.next()) {
                writer.write(set.getInt("ID")+" ");
                writer.write(set.getString("PRICE")+"");
                writer.write("<br>");
            }
            writer.println("</body></html");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
